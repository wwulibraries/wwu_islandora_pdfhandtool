# Overview
This module changes the PDF.js default cursor tool from pointer to the hand tool

# Alternative
You could also accomplish this by changing enableHandToolOnLoad: false to enableHandToolOnLoad: true in viewer.js.
Thanks to https://stackoverflow.com/questions/22044234/in-pdf-js-how-do-i-turn-on-handtool-as-the-default-setting 

# Installation
- navigate to your modules directory (usually /var/www/drupal/sites/all/modules/)
- clone this repository into that folder (e.g, 
git clone https://bitbucket.org/wwulibraries/wwu_islandora_pdfhandtool.git)
- drush en wwu_islandora_pdfhandtool

# Author: 
David Bass @ WWU

# License: 
MIT
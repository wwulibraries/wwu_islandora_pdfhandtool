// https://bitbucket.org/wwulibraries/wwu_islandora_pdfhandtool/

// window.addEventListener('load', function() {
jQuery(window).load(function(){

    if (document.getElementById('islandora-pdfjs')) {

        function checkStatus() {
            // console.log('checking status');
            var pdfStatus = document.getElementById('islandora-pdfjs').getElementsByTagName('iframe')[0].contentWindow.PDFViewerApplication.downloadComplete;
            // console.log('pdfStatus', pdfStatus);
            if (pdfStatus) {
                activateHandTool();
                clearInterval(checkStatusInterval);
            }
        }

        var checkStatusInterval = setInterval ( checkStatus, 500 );     //  check the status every 1/2 second
    
        function activateHandTool() {
            if (document.getElementById('islandora-pdfjs').getElementsByTagName('iframe')[0]) {
                document.getElementById('islandora-pdfjs').getElementsByTagName('iframe')[0].contentWindow.PDFViewerApplication.pdfCursorTools.handTool.activate();
                // alternative techniques described in  https://stackoverflow.com/questions/22044234/in-pdf-js-how-do-i-turn-on-handtool-as-the-default-setting
            }
        }
    }    
})